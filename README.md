# Questions / Answer API

This is a quarter completed API to save a users email, get a users email, get a list of question+answers and save the selected question answers written in ASP.NET Core and Entity Framework Core.  

## Setup

If .Net Core is installed, should just need to run a couple commands to get the API running

```
dotnet restore
dotnet watch run
```

## DB

For the sake of a quick prototype, I used SQLite, which is included in the repo. It is easy to change the database used to Postgres, MySQL or MS SQL. 

## Missing Things

### Tests
I've been more into TDD but did not include any tests for this.

### Complete and through error handling
There are some pieces that need to be added to handle different errors that come up 

### Swagger
Quality documentation