﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using backend.EFQA;

namespace backend.Controllers
{

    [Route("user")]
    [ApiController]
    public class UserController : ControllerBase {
        [HttpGet("{userEmailLookup}")]
        public ActionResult<Users> Get(string userEmailLookup) {
            Users userExists = new Users();
            using(var client = new questionsContext()) {
                userExists = client.Users.SingleOrDefault(x => x.Email == userEmailLookup);
                if(userExists == null) {
                    return NotFound(userEmailLookup);
                }
            }
            return Ok(userExists);
        }

        [HttpPost]
        public ActionResult<Users> createNewUser(Users newUserEmail) {
            using(var client = new questionsContext()) {
                if(client.Users.SingleOrDefault(x => x.Email == newUserEmail.Email) != null) {
                    return StatusCode(StatusCodes.Status422UnprocessableEntity, "User already exists");
                }
                newUserEmail.Guid = Guid.NewGuid().ToString();
                client.Users.Add(newUserEmail);
                client.SaveChanges();
                return Ok(newUserEmail);
            }
        }
    }
}
