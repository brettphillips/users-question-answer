using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using backend.EFQA;

namespace backend.Controllers 
{
    [Route("answer")]
    [ApiController]
    public class AnswerController : ControllerBase {
        [HttpPost]
        public ActionResult<UserAnswers> Post(UserAnswers ua) {
            Console.WriteLine(ua);
            int res = 0;
            using(var client = new questionsContext()) {
                UserAnswers insertOrUpdate = client.UserAnswers.SingleOrDefault(x => x.QuestionNumber == ua.QuestionNumber && x.UserGuid == ua.UserGuid);
                if(insertOrUpdate == null) {
                    client.UserAnswers.Add(ua);
                } else {
                    insertOrUpdate.AnswerKey = ua.AnswerKey;
                    client.UserAnswers.Update(insertOrUpdate);
                }
                try {
                    res = client.SaveChanges();
                } catch {
                    return NotFound();
                }
            }
            return ua;
        }
    }
}