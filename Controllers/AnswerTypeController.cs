using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using backend.EFQA;

namespace backend.Controllers 
{
    [Route("answerType")]
    [ApiController]
    public class AnswerTypeController : ControllerBase {
        [HttpGet]
        public Dictionary<long, List<AnswerType>> Get() {
            List<AnswerType> at = new List<AnswerType>();
            using(var client = new questionsContext()) {
                at = client.AnswerType.ToList();
            }

            System.Console.WriteLine(at);

            Dictionary<long, List<AnswerType>> answerDict = new Dictionary<long, List<AnswerType>>();
            at.ForEach( ans => {
                if(answerDict.ContainsKey(ans.Number.Value)) {
                    answerDict[ans.Number.Value].Add(ans);
                } else {
                    answerDict.Add(ans.Number.Value, new List<AnswerType>(){ans});
                }
            });
            return answerDict;
        }
    }
}