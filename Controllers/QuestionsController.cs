using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using backend.EFQA;
using backend.Classes;

namespace backend.Controllers 
{
    [Route("question")]
    [ApiController]
    public class QuestionController : ControllerBase 
    {
        private readonly questionsContext _context;

        [HttpGet]
        public List<Questions> Get() 
        {   
            List<Questions> questions = new List<Questions>();
            using(var client = new questionsContext()) {
                questions = client.Questions.ToList();
            }
            return questions;
        }

        [HttpGet("group")]
        public dynamic GetGroup() 
        {   
            List<QuestionsGrouped> questionsInAGroup = new List<QuestionsGrouped>();
            dynamic dynamicQuestions;
            using(var client = new questionsContext()) {
                List<Questions> questions = client.Questions.ToList();
                List<QuestionHeader> questionHeader = client.QuestionHeader.ToList();
                var questionGroup = from q in questions join qh in questionHeader on q.QuestionHeader equals qh.Number into qhs 
                                    from qh in qhs.DefaultIfEmpty()
                                    select new QuestionsGrouped(q.Number,q.QuestionText, q.QuestionTextHelper, (long) q.AnswerType, qh == null ? -1 : qh.Number);
                questionsInAGroup = questionGroup.ToList();
                dynamicQuestions = from qg in questionGroup
                                    group new {qg.QuestionNumber, qg.QuestionText, qg.QuestionTextHelper, qg.AnswerType} by qg.QuestionHeader into qgYES
                                    select new {QuestionNum = qgYES.Key, Results = qgYES.ToList()};
            }
            return dynamicQuestions;
        }

        [HttpGet("{userGuid:guid}")]
        public List<QuestionAnswerResults> GetQuestionsUserCompleted(Guid userGuid) 
        {   
            List<QuestionAnswerResults> qaobjList = new List<QuestionAnswerResults>();
            using(var client = new questionsContext()) {
                List<UserAnswers> questionsAnswered = client.UserAnswers.Where(x => x.UserGuid == userGuid.ToString()).ToList();
                List<Questions> questions = client.Questions.ToList();
                var questionAndAnswer = from q in questions 
                        join qa in questionsAnswered 
                        on q.Number equals qa.QuestionNumber into qas 
                        from qa in qas.DefaultIfEmpty() 
                        select new QuestionAnswerResults(qa == null ? -1 : (long) qa.AnswerKey, q.QuestionText, q.Number, (long) q.AnswerType, q.QuestionTextHelper);
                        qaobjList = questionAndAnswer.ToList();
            }
            return qaobjList;
        }

        [HttpGet("{questionNumber:int}")]
        public Questions Get(int questionNumber) 
        {   
            Questions question;
            using(var client = new questionsContext()) {
                question = client.Questions
                        .Where(a => a.Number == questionNumber)
                        .DefaultIfEmpty(new Questions(0,"No question found", 0))
                        .Single();
            }
            return question;
        }
    }
}