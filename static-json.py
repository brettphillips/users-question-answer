import json
import sqlite3
import sys

class Question:
    def __init__(self, qt, qa):
        self.QuestionText = qt
        self.QuestionAnswer = qa

def main():
    try:
        conn = sqlite3.connect("questions.db")
        conn.row_factory = sqlite3.Row
        with conn:
            cur = conn.cursor()
            cur.execute("SELECT QuestionText, AnswerType, QuestionTextHelper FROM questions")
            rows = cur.fetchall()

            allQA = []
            for row in rows:
                allQA.append(Question(row["questiontext"], row["answertype"]))
            # jsonT = json.load(allQA)
            print(allQA)
    except:
        print("Yep, this fixed it %1", (sys.exc_info()[0],))

if __name__ == "__main__":
    main()