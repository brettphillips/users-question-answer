﻿using System;
using System.Collections.Generic;

namespace backend.EFQA
{
    public partial class Questions
    {
        public long Number { get; set; }
        public string QuestionText { get; set; }
        public long? AnswerType { get; set; }
        public string QuestionTextHelper { get; set; }
        public long? QuestionHeader { get; set; }
    }
}
