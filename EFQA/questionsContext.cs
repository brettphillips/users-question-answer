﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace backend.EFQA
{
    public partial class questionsContext : DbContext
    {
        public questionsContext()
        {
        }

        public questionsContext(DbContextOptions<questionsContext> options)
            : base(options)
        {
        }

        public virtual DbSet<AnswerType> AnswerType { get; set; }
        public virtual DbSet<AnswertypeOld> AnswertypeOld { get; set; }
        public virtual DbSet<QuestionHeader> QuestionHeader { get; set; }
        public virtual DbSet<Questions> Questions { get; set; }
        public virtual DbSet<UserAnswers> UserAnswers { get; set; }
        public virtual DbSet<UserAnswersOld> UserAnswersOld { get; set; }
        public virtual DbSet<Users> Users { get; set; }

        // Unable to generate entity type for table 'userAnswers_log'. Please see the warning messages.

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlite("DataSource=questions.db");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AnswerType>(entity =>
            {
                entity.HasKey(e => e.Index);

                entity.Property(e => e.Index).ValueGeneratedNever();
            });

            modelBuilder.Entity<AnswertypeOld>(entity =>
            {
                entity.HasKey(e => e.Idx);

                entity.ToTable("answertype_old");

                entity.Property(e => e.Idx).ValueGeneratedNever();
            });

            modelBuilder.Entity<QuestionHeader>(entity =>
            {
                entity.HasKey(e => e.Number);

                entity.ToTable("questionHeader");

                entity.Property(e => e.Number).ValueGeneratedNever();
            });

            modelBuilder.Entity<Questions>(entity =>
            {
                entity.HasKey(e => e.Number);

                entity.ToTable("questions");

                entity.Property(e => e.Number).ValueGeneratedNever();
            });

            modelBuilder.Entity<UserAnswers>(entity =>
            {
                entity.HasKey(e => new { e.QuestionNumber, e.UserGuid });

                entity.ToTable("userAnswers");
            });

            modelBuilder.Entity<UserAnswersOld>(entity =>
            {
                entity.HasKey(e => new { e.QuestionNumber, e.AnswerKey, e.UserGuid });

                entity.ToTable("userAnswers_old");
            });

            modelBuilder.Entity<Users>(entity =>
            {
                entity.HasKey(e => e.Guid);

                entity.ToTable("users");

                entity.Property(e => e.Guid)
                    .HasColumnName("guid")
                    .ValueGeneratedNever();

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasColumnName("email");
            });
        }
    }
}
