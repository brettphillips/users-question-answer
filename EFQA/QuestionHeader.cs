﻿using System;
using System.Collections.Generic;

namespace backend.EFQA
{
    public partial class QuestionHeader
    {
        public long Number { get; set; }
        public string Text { get; set; }
    }
}
