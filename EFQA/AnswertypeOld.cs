﻿using System;
using System.Collections.Generic;

namespace backend.EFQA
{
    public partial class AnswertypeOld
    {
        public long Idx { get; set; }
        public long? Number { get; set; }
        public string Answer { get; set; }
        public string AnswerDescription { get; set; }
    }
}
