﻿using System;
using System.Collections.Generic;

namespace backend.EFQA
{
    public partial class UserAnswers
    {
        public long QuestionNumber { get; set; }
        public long? AnswerKey { get; set; }
        public string UserGuid { get; set; }
    }
}
