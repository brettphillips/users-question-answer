﻿using System;
using System.Collections.Generic;

namespace backend.EFQA
{
    public partial class UserAnswersOld
    {
        public long QuestionNumber { get; set; }
        public long AnswerKey { get; set; }
        public string UserGuid { get; set; }
    }
}
