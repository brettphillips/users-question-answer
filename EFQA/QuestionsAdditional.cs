using System;
using System.Collections.Generic;

namespace backend.EFQA
{
    public partial class Questions {
        public Questions(long num, string qt, long at) {
            this.Number = num;
            this.QuestionText = qt;
            this.AnswerType = at;
        }

        public Questions() {
            
        }
    }
}