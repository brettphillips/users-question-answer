namespace backend.Classes{
    public class QuestionAnswerResults {
        public long QuestionAnswerSelection { get; set; }
        public string QuestionText { get; set; }
        public long Number { get; set; }
        public long AnswerType { get; set; }
        public string QuestionTextHelper { get; set; }
        public QuestionAnswerResults() { }
        /// <summary>
        /// qas: Answer to the selected question
        /// qt: Text of the selected question
        /// qn: Number (id really) of the selected question
        /// at: Number (id really) of the answer type
        /// </summary>
        public QuestionAnswerResults(long qas, string qt, long qn, long at, string qth) {
            this.QuestionAnswerSelection = qas;
            this.QuestionText = qt;
            this.Number = qn;
            this.AnswerType = at;
            this.QuestionTextHelper = qth;
        }
    }
}