

namespace backend.Classes{
    public class QuestionsGrouped {
        public long QuestionNumber { get; set; }
        public string QuestionText { get; set; }
        public string QuestionTextHelper { get; set; }
        public long AnswerType { get; set; }
        public long QuestionHeader { get; set; }
        
        /// <summary>
        /// qn: Number (id really) of the selected question
        /// qt: Text of the selected question
        /// qth: question text helper info
        /// at: Number (id really) of the answer type
        /// qh: question header ID, if exists
        /// </summary>
        public QuestionsGrouped(long qn, string qt, string qth, long at, long qh) {
            this.QuestionNumber = qn;
            this.QuestionText = qt;
            this.QuestionTextHelper = qth;
            this.AnswerType = at;
            this.QuestionHeader = qh;
        }
    }
}